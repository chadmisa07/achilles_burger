from fb_chatbot import models
from django.core.serializers.json import Serializer
from django.core import serializers
import json, time

class AttachmentSerializer(Serializer):
	def get_dump_object(self, obj):
		mapped_object = {
			"attachment":{
				"type":obj.attachment_type.attachment_type,
				"payload":{
					"url":obj.url
				}
			}
		}
		return mapped_object


class SurveySerializer(Serializer):
	def get_dump_object(self, obj):
		mapped_object = {
			'survey template': obj.description
		}
		return mapped_object


class TextMessageSerializer(Serializer):
	def get_dump_object(self, obj):
		mapped_object = {
			"text":obj.text
		}
		return mapped_object


class QuickReplyTextSerializer(Serializer):
	def get_dump_object(self, obj):
		if obj.image_url == "":
			mapped_object = {
				"content_type":obj.content_type,
				"title":obj.title,
				"payload":obj.payload
			}
		else:
			mapped_object = {
				"content_type":obj.content_type,
				"title":obj.title,
				"payload":obj.payload,
				"image_url":obj.image_url
			}
		return mapped_object

class SurveyTextSerializer(Serializer):
	def get_dump_object(self, obj):
		if obj.image_url == "":
			mapped_object = {
				"content_type":obj.content_type,
				"title":obj.title,
				"payload":obj.payload
			}
		else:
			mapped_object = {
				"content_type":obj.content_type,
				"title":obj.title,
				"payload":obj.payload,
				"image_url":obj.image_url
			}
		return mapped_object


class QuickReplyLocationSerializer(Serializer):
	def get_dump_object(self, obj):
		mapped_object = {
			"content_type":obj.content_type
		}
		return mapped_object

class SurveyLocationSerializer(Serializer):
	def get_dump_object(self, obj):
		mapped_object = {
			"content_type":obj.content_type
		}
		return mapped_object


class QuickReplyResponseSerializer(Serializer):
	def get_dump_object(self, obj):
		loc_serializer = QuickReplyLocationSerializer()
		qr_location = models.QuickReplyLocation.objects.filter(quick_reply_response=obj)
		qr_location = loc_serializer.serialize(qr_location)
		text_serializer = QuickReplyTextSerializer()
		qr_text = models.QuickReplyText.objects.filter(quick_reply_response=obj)
		qr_text = text_serializer.serialize(qr_text)
		mapped_object = {
			"text":obj.text,
			"quick_replies": json.loads(qr_location) + json.loads(qr_text)
		}
		return mapped_object

class SurveyResponseSerializer(Serializer):
	def get_dump_object(self, obj):
		loc_serializer = SurveyLocationSerializer()
		qr_location = models.SurveyReplyLocation.objects.filter(survey_question=obj)
		qr_location = loc_serializer.serialize(qr_location)
		text_serializer = SurveyTextSerializer()
		qr_text = models.SurveyReplyText.objects.filter(survey_question=obj)
		qr_text = text_serializer.serialize(qr_text)
		mapped_object = {
			"text":obj.text,
			"quick_replies": json.loads(qr_location) + json.loads(qr_text)
		}
		return mapped_object


class PostbackButtonSerializer(Serializer):
	def get_dump_object(self, obj):
		mapped_object = {
			"type":obj.button_type,
			"title":obj.title,
			"payload":obj.payload
		}
		return mapped_object



class ButtonSerializer(Serializer):
	def get_dump_object(self, obj):
		if (obj.button_type == 'element_share'):
			mapped_object = {
				"type":obj.button_type
			}
		elif (obj.button_type == 'web_url'):
			mapped_object = {
				"type":obj.button_type,
				"url":obj.url,
				"title":obj.title,
				"webview_height_ratio":obj.webview_height_ratio.size,
			}
		else:
			mapped_object = {
				"type":obj.button_type,
				"title":obj.title,
				"payload":obj.payload
			}
		return mapped_object


class ButtonResponseSerializer(Serializer):
	def get_dump_object(self, obj):
		serializer = ButtonSerializer()
		postback = models.ButtonResponsePostbackButton.objects.filter(button_response=obj)
		postback = serializer.serialize(postback)
		url = models.ButtonResponseURLButton.objects.filter(button_response=obj)
		url = serializer.serialize(url)
		call = models.ButtonResponseCallButton.objects.filter(button_response=obj)
		call = serializer.serialize(call)
		mapped_object = {
			"attachment": {
				"type":"template",
				"payload":{
					"template_type":"button",
					"text":obj.text,
					"buttons": json.loads(postback) + json.loads(url) + json.loads(call)
				}
			}
		}
		return mapped_object


class GenericTemplateSerializer(Serializer):
	def get_dump_object(self, obj):
		serializer = ButtonSerializer()
		postback = models.GenericTemplatePostbackButton.objects.filter(generic_template=obj)
		postback = serializer.serialize(postback)
		url = models.GenericTemplateURLButton.objects.filter(generic_template=obj)
		url = serializer.serialize(url)
		call = models.GenericTemplateCallButton.objects.filter(generic_template=obj)
		call = serializer.serialize(call)
		share = models.GenericTemplateShareButton.objects.filter(generic_template=obj)
		share = serializer.serialize(share)
		mapped_object = {
			"title":obj.title,
			"item_url":obj.item_url,
			"image_url":obj.image_url,
			"subtitle":obj.subtitle,
			"buttons": json.loads(postback) + json.loads(url) + json.loads(call) + json.loads(share)
		}
		return mapped_object


class GenericTemplateResponseSerializer(Serializer):
	def get_dump_object(self, obj):
		serializer = GenericTemplateSerializer()
		gen_templates = serializer.serialize(obj.elements.all())
		mapped_object = {
			"attachment":{
				"type":"template",
				"payload":{
					"template_type":"generic",
					"elements":json.loads(gen_templates)
				}
			}
		}
		return mapped_object


class AddressSerializer(Serializer):
	def get_dump_object(self, obj):
		mapped_object = {
			'street_1' : obj.street_1,
			'street_2' : obj.street_2,
			'city' : obj.city,
			"postal_code": obj.postal_code,
			"state": obj.state,
			"country" : obj.country
		}
		return mapped_object


class ReceiptElementSerializer(Serializer):
	def get_dump_object(self, obj):
		mapped_object = {
			'title' : obj.title,
			'subtitle' : obj.subtitle,
			'quantity' : obj.quantity,
			"price": obj.price,
			"currency": obj.currency,
			"image_url" : obj.image_url
		}
		return mapped_object


class AdjustmentSerializer(Serializer):
	def get_dump_object(self, obj):
		mapped_object = {
			'name' : obj.name,
			'amount' : obj.amount
		}
		return mapped_object


class ReceiptResponseSerializer(Serializer):
	def get_dump_object(self, obj):
		serializer = ReceiptElementSerializer()
		elements_serialized_data = serializer.serialize(obj.elements.all())
		elements_serialized_data = json.loads(elements_serialized_data)
		serializer = AddressSerializer()
		address_serialized_data = serializer.serialize([obj.recipient_address])
		address_serialized_data = json.loads(address_serialized_data)
		serializer = AdjustmentSerializer()
		adjustments_serialized_data = serializer.serialize(obj.adjustments.all())
		adjustments_serialized_data = json.loads(adjustments_serialized_data)
		mapped_object = {
			'attachment' : {
				'type' : 'template',
				'payload' : {
					"template_type":"receipt",
					"recipient_name": obj.recepient_name, 
					"order_number": obj.order_number,
					"currency": obj.currency,
					"payment_method": obj.payment_method,
					"order_url" : obj.order_url,
					"timestamp" : int(time.mktime(obj.timestamp.timetuple())),
					"elements" : elements_serialized_data,
					"address" : address_serialized_data[0],
					"summary" : {
						'subtotal' : obj.subtotal,
						'shipping_cost' : obj.shipping_cost,
						'total_tax' : obj.total_tax,
						'total_cost' : obj.total_cost
					},
					"adjustments" : adjustments_serialized_data
				}
			}
		}
		return mapped_object

