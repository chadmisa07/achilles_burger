from django.contrib import admin
from fb_chatbot import models

# Facebook Profiles Admin Settings
class FacebookUserAdmin(admin.ModelAdmin):
	search_fields = "first_name","last_name", 
	list_display = "first_name","last_name", "gender", "timezone", "locale"
	list_filter = "first_name", "last_name", "gender", "timezone", "locale",
	list_per_page = 10


 # Attachment Admin Settings
class AttachmentAdmin(admin.ModelAdmin):
	search_fields = "title", "attachment_type",
	list_filter = "title", "attachment_type",
	list_per_page = 10


# Quick Reply Admin Settings
class QuickReplyTextInline(admin.TabularInline):
    model = models.QuickReplyText
    extra = 0

class SurveyReplyTextInline(admin.TabularInline):
	model = models.SurveyReplyText
	extra = 0

class SurveyReplyLocationInline(admin.TabularInline):
	model = models.SurveyReplyLocation
	readonly_fields = 'content_type',
	extra = 0

class SurveyQuestionInline(admin.TabularInline):
	model = models.SurveyQuestion
	search_fields = "text", 
	list_per_page = 10

class SurveyQuestionAdmin(admin.ModelAdmin):
	inlines = (SurveyReplyTextInline, SurveyReplyLocationInline)
	extra = 0

class QuickReplyLocationInline(admin.TabularInline):
	model = models.QuickReplyLocation
	readonly_fields = 'content_type',
	extra = 0

class QuickReplyResponseAdmin(admin.ModelAdmin):
	inlines = (QuickReplyTextInline, QuickReplyLocationInline)
	search_fields = "text", 
	list_per_page = 10

# Button Response Admin Settings
class ButtonResponsePostbackButtonInline(admin.TabularInline):
	model = models.ButtonResponsePostbackButton
	extra = 0

class ButtonResponseCallButtonInline(admin.TabularInline):
	model = models.ButtonResponseCallButton
	extra = 0

class ButtonResponseURLButtonInline(admin.TabularInline):
	model = models.ButtonResponseURLButton
	extra = 0

class ButtonResponseAdmin(admin.ModelAdmin):
	inlines = (ButtonResponsePostbackButtonInline, ButtonResponseCallButtonInline, 
		ButtonResponseURLButtonInline)
	search_fields = "text", 
	list_per_page = 10

# Generic Template Admin Settings
class GenericTemplatePostbackButtonInline(admin.TabularInline):
	model = models.GenericTemplatePostbackButton
	extra = 0

class GenericTemplateCallButtonInline(admin.TabularInline):
	model = models.GenericTemplateCallButton
	extra = 0

class GenericTemplateShareButtonInline(admin.TabularInline):
	model = models.GenericTemplateShareButton
	readonly_fields = 'button_type',
	extra = 0

class GenericTemplateURLButtonInline(admin.TabularInline):
	model = models.GenericTemplateURLButton
	extra = 0

class GenericTemplateResponseAdmin(admin.ModelAdmin):
	inlines = (GenericTemplatePostbackButtonInline, GenericTemplateCallButtonInline, 
		GenericTemplateShareButtonInline, GenericTemplateURLButtonInline)
	list_display = ("title","subtitle")
	search_fields = "title",
	list_per_page = 10

class ProductItemAdmin(admin.ModelAdmin):
	list_display = ("name", "code", "price", "category")
	list_filter = ("category",)
	search_fields = "name",
	list_per_page = 15



# Remove Comments to see it in Admin
# Or comment to Remove
admin.site.register(models.Address)
admin.site.register(models.Adjustment)
admin.site.register(models.Attachment, AttachmentAdmin)
admin.site.register(models.AttachmentType)
admin.site.register(models.ButtonResponse, ButtonResponseAdmin)
admin.site.register(models.ButtonResponseCallButton)
admin.site.register(models.ButtonResponsePostbackButton)
admin.site.register(models.ButtonResponseURLButton)
admin.site.register(models.ButtonResponseURLButtonHeight)
admin.site.register(models.Keyword)
admin.site.register(models.FacebookUser, FacebookUserAdmin)
admin.site.register(models.GenericTemplate, GenericTemplateResponseAdmin)
admin.site.register(models.GenericTemplateCallButton)
admin.site.register(models.GenericTemplatePostbackButton)
admin.site.register(models.GenericTemplateResponse)
admin.site.register(models.GenericTemplateURLButton)
admin.site.register(models.GenericTemplateURLButtonHeight)
admin.site.register(models.QuickReplyLocation)
admin.site.register(models.QuickReplyResponse, QuickReplyResponseAdmin)
admin.site.register(models.QuickReplyText)
admin.site.register(models.SurveyQuestion, SurveyQuestionAdmin)
admin.site.register(models.SurveyTemplate)
admin.site.register(models.SurveyReplyLocation)
admin.site.register(models.SurveyReplyText)
admin.site.register(models.ReceiptElement)
admin.site.register(models.ReceiptResponse)
admin.site.register(models.TextMessage)
admin.site.register(models.PreviousKeyword)
admin.site.register(models.ProductCategory)
admin.site.register(models.ProductItem, ProductItemAdmin)
admin.site.register(models.CartItem)
admin.site.register(models.ProductOrder)
admin.site.register(models.VirtualCart)
admin.site.register(models.ProductCategoryOrder)