import json, random, requests
from pprint import pprint
from django.views import generic
from django.http.response import HttpResponse
from django.shortcuts import render
from django.views.decorators.csrf import csrf_exempt
from django.utils.decorators import method_decorator
from random import randint
from fb_chatbot import models
from fb_chatbot import serializers
from django.core.exceptions import ValidationError
from django.core.validators import validate_email
from django.conf import settings
from django.core.mail import EmailMultiAlternatives

def view_cart(request):
	try:
		user = models.FacebookUser.objects.get(messenger_id=1209592182413224)
		cart = models.VirtualCart.objects.get(user=user)
		items = cart.products_added.all()
		context = {
			"items":items
		}
	except:
		context = {}
	return render(request, 'fb_chatbot/view_cart.html', context)

class FacebookChatbot(generic.View):

	access_token = "EAADwV8yDKaMBAJpD48ZBAZAKcZB52gHgOybACyCAPZCC1GEekZAkTPLCGZB5ZBHZCwPOO5sprqvzcm88Sty6SgWqc8ecGFP3oGCd8FjlqIZAMCRHRUzXGsvBL2wfLVslrO0qXKrKezXgkN2YaA3RhtEYwKzRNlSnmfvhWklrvdtAW0D5WtFjR8kq9"
	hub_challenge = "1717708666"
	verify_token = "fb_chatbot_chad"

	@method_decorator(csrf_exempt)
	def dispatch(self, request, *args, **kwargs):
		return generic.View.dispatch(self, request, *args, **kwargs)

	def get(self, request, *args, **kwargs):
		if self.request.GET['hub.verify_token'] == self.verify_token:
			print("Hub Challenge >>>>>> " + self.request.GET['hub.challenge'])
			return HttpResponse(self.request.GET['hub.challenge'])
		else:
			return HttpResponse('Error, invalid token')

	def post(self, request, *args, **kwargs):
		incoming_message = json.loads(self.request.body.decode('utf-8'))
		print(incoming_message)
		for entry in incoming_message['entry']:
			for message in entry['messaging']:
				sender = message['sender']['id']
				if "delivery" in message:
					pass
				elif "read" in message:
					pass
				elif 'postback' in message:
					postback = message['postback']['payload']
					print('POSTBACK > ' + postback)
					self.parse_intent(sender, postback.lower())
				elif 'message' in message:
					if "is_echo" in message['message']:
						pass
					elif "quick_reply" in message['message']:
						message = message['message']['quick_reply']['payload']
						print('MESSAGE > ' + message)
						self.parse_intent(sender, message.lower())
					elif "attachments" in message['message']:
						print("Has attachments")
						attachment = message['message']['attachments']
						if attachment[0]['type'] == 'location':
							print("Location Type!")
							self.send_response(sender, 'c0mm@nd_other_stores')
					elif "text" in message['message']:			
						message = message['message']['text']
						print('MESSAGE > ' + message)
						self.parse_intent(sender, message.lower())
				
		return HttpResponse()

	def save_keyword(self, sender, received_message):
		new_keyword = models.PreviousKeyword(user=sender, key_word=received_message)
		new_keyword.save()
		print("key word saved!!!")

	def email_sender(self, sender, recepient):
		try:
			sender_obj = models.FacebookUser.objects.get(messenger_id=sender)
			cart = models.VirtualCart.objects.get(user=sender_obj)
			products_added = cart.products_added.all()
			receipt = models.ReceiptResponse()
			receipt.recepient_name = sender_obj.first_name + " " + sender_obj.last_name
			receipt.order_number = random.randint(111111,999999)
			receipt.payment_method = 'Visa'
			receipt.recipient_address = models.Address.objects.get(pk=3)
			receipt.shipping_cost = 50
			receipt.total_tax = 50
			adjustments = models.Adjustment.objects.all()
			total_discounts = self.get_total_adjustments(adjustments)
			total_costs = self.get_total_cost(products_added)
			receipt.subtotal = total_costs
			receipt.total_cost = total_costs - total_discounts + receipt.total_tax + receipt.shipping_cost
			if receipt.total_cost < 0:
				receipt.total_cost = 1

			html_content = '<h3>Order Details</h3>'
			html_content = html_content + '<table style="border-collapse:collapse;border: 1px solid black;"><th style="border: 1px solid black;">Product Name</th><th style="border: 1px solid black;">Quantity</th><th style="border: 1px solid black;">Price</th>'
			for added_item in products_added:
				html_content = html_content + '<tr><td style="border: 1px solid black;">' + str(added_item.item.name) + '</td><td style="border: 1px solid black;">' + str(added_item.quantity) + '</td><td style="border: 1px solid black;">P' + str(added_item.item.price) + '</td></tr>'

			html_content = html_content + '</table><br>'
			html_content = html_content + '<table>'
			html_content = html_content + '<tr><td width=200>Subtotal</td><td>P'+str(total_costs)+'</tr>'
			html_content = html_content + '<tr><td width=200>Shipping & Handling</td><td>P'+str("%.2f" % receipt.shipping_cost)+'</tr>'
			html_content = html_content + '<tr><td width=200>Online Order Discount</td><td>P'+str(total_discounts)+'</tr>'
			html_content = html_content + '<tr><td width=200>Estimated Tax</td><td>P'+str("%.2f" % receipt.total_tax)+'</tr>'
			html_content = html_content + '<tr><td width="200" style="border-top: 1px solid black;"><b>Total Cost</b></td><td style="border-top: 1px solid black;"><b>P'+str(receipt.total_cost)+'</b></tr>'
			html_content = html_content + '</table>'

			subject, from_email, to = 'Jollibee Online Purchase Receipt',settings.EMAIL_HOST_USER, recepient
			text_content = 'This is an important message.'
			
			msg = EmailMultiAlternatives(subject, text_content, from_email, [to])
			msg.attach_alternative(html_content, "text/html")
			msg.send()
			self.send_text_message(sender, "Receipt already sent to your email.")

			print("SUCCESSFULLY SEND EMAIL")
		except:
			print("ERROR SENDING EMAIL")


	def send_response(self, sender, received_message):
		try:
			keyword = models.Keyword.objects.get(key_word=received_message)

			text_messages = keyword.text_message.all()
			for text_message in text_messages:
				text_serializer = serializers.TextMessageSerializer()
				serialized_text_message = text_serializer.serialize([text_message])
				serialized_text_message = json.loads(serialized_text_message)
				self.send_message(sender, serialized_text_message[0])

			attachments = keyword.attachment.all()
			for attachment in attachments:
				attachment_serializer = serializers.AttachmentSerializer()
				serialized_attachment = attachment_serializer.serialize([attachment])
				serialized_attachment = json.loads(serialized_attachment)
				self.send_message(sender, serialized_attachment[0])

			generic_responses = keyword.generic_response.all()
			print(generic_responses)
			for generic_response in generic_responses:
				generic_response_serializer = serializers.GenericTemplateResponseSerializer()
				serialized_generic_response = generic_response_serializer.serialize([generic_response])
				serialized_generic_response = json.loads(serialized_generic_response)
				self.send_message(sender, serialized_generic_response[0]) 

			quick_replies = keyword.quick_reply.all()
			for quick_reply in quick_replies:
				qr_serializer = serializers.QuickReplyResponseSerializer()
				serialized_quick_reply = qr_serializer.serialize([quick_reply])
				serialized_quick_reply = json.loads(serialized_quick_reply)
				self.send_message(sender, serialized_quick_reply[0])    

			button_responses = keyword.button_response.all()
			for button_response in button_responses:
				button_serializer = serializers.ButtonResponseSerializer()
				serialized_button_response = button_serializer.serialize([button_response])
				serialized_button_response = json.loads(serialized_button_response)
				self.send_message(sender, serialized_button_response[0])

			receipt_responses = keyword.receipt_response.all()
			for receipt_response in receipt_responses:
				receipt_response_serialzer = serializers.ReceiptResponseSerializer()
				serialized_receipt_response = receipt_response_serialzer.serialize([receipt_response])
				serialized_receipt_response = json.loads(serialized_receipt_response)
				self.send_message(sender, serialized_receipt_response[0])
			self.save_keyword(sender, received_message)

			# survey_responses = keyword.survey.all()
			# for survey_response in survey_responses:
			# 	self.send_message(sender, {"text":"SSSUUURRRVVVEEEYYY!!!"})
			# 	survey_serializer = serializers.SurveyReplyResponseSerializer()
			# 	serialized_survey_response = survey_serializer.serialize([survey_response])
		except:
			self.send_message(sender, {"text":"Sorry. We are unable to process your request at the moment."})

	def send_message(self, fbid, message):
		post_message_url = 'https://graph.facebook.com/v2.6/me/messages?access_token=' + self.access_token
		response_msg = json.dumps({"recipient":{"id":fbid}, "message":message})
		status = requests.post(post_message_url, headers={"Content-Type": "application/json"},data=response_msg)

	def send_text_message(self, fbid, message):
		self.send_message(fbid, {"text":message})

	def add_to_cart(self, sender, code, amount):
		try:
			product_item = models.ProductItem.objects.get(code=code)
			sender_obj = models.FacebookUser.objects.get(messenger_id=sender)
			cart_item = models.CartItem(quantity=amount)
			cart_item.item = product_item
			try:
				cart = models.VirtualCart.objects.get(user=sender_obj)
				try:
					cart_items = cart.products_added.get(item=product_item)
					cart_items.quantity += amount
					cart_items.save()
					self.send_message(sender, {'text' : 'Order updated to ' + str(cart_items.quantity) + '.'})
				except:
					cart_item.save()
					cart.products_added.add(cart_item)
					self.send_text_message(sender, 'Item added to cart')
			except:
				cart = models.VirtualCart(user=sender_obj)
				cart.save()
				cart_item.save()
				cart.products_added.add(cart_item)
				self.send_text_message(sender, 'Item added')
			cart.save()
			self.send_response(sender, "c0mm@nd_sh0w_0pti0ns")
		except:
			self.send_message(sender, {'text' : 'Please enter a valid value.'})

	def get_total_adjustments(self, adjustments):
		total_adjustment = 0
		for adjustment in adjustments:
			total_adjustment += adjustment.amount
		return total_adjustment

	def get_total_cost(self, items):
		total_cost = 0
		for item in items:
			total_cost += item.item.price * item.quantity
		return total_cost

	def do_check_out(self, sender):
		try:
			sender_obj = models.FacebookUser.objects.get(messenger_id=sender)
			cart = models.VirtualCart.objects.get(user=sender_obj)
			products_added = cart.products_added.all()
			receipt = models.ReceiptResponse()
			receipt.recepient_name = sender_obj.first_name + " " + sender_obj.last_name
			receipt.order_number = random.randint(111111,999999)
			receipt.payment_method = 'Visa'
			receipt.recipient_address = models.Address.objects.get(pk=3)
			receipt.shipping_cost = 50
			receipt.total_tax = 50
			adjustments = models.Adjustment.objects.all()
			total_discounts = self.get_total_adjustments(adjustments)
			total_costs = self.get_total_cost(products_added)
			receipt.subtotal = total_costs
			receipt.total_cost = total_costs - total_discounts + receipt.total_tax + receipt.shipping_cost
			if receipt.total_cost < 0:
				receipt.total_cost = 1
			receipt.save()
			for adjustment in adjustments:
				receipt.adjustments.add(adjustment)
			for added_item in products_added:
				product_order = models.ProductOrder(user=sender_obj, item=added_item.item, quantity=added_item.quantity)
				product_order.save()
				element = models.ReceiptElement()
				element.title = added_item.item.name
				element.subtitle = added_item.item.description
				element.quantity = added_item.quantity
				element.price = added_item.item.price
				element.image_url = added_item.item.image_url
				element.save()
				receipt.elements.add(element)
			receipt.save()
			serializer = serializers.ReceiptResponseSerializer()
			data = serializer.serialize([receipt])
			data = json.loads(data)
			self.send_message(sender, data[0])
			cart.delete()
			self.send_text_message(sender, "Thank you for your purchase. If you have more concerns, press the 3 horizontal buttons on the lower left.")
		except:
			self.send_message(sender, {'text' : 'Cart is empty. Unable to proceed with checkout.'})
			self.send_response(sender, "c0mm@nd_sh0w_0pti0ns")


	def survey(self,sender,message):
		keyword = models.Keyword.objects.get(key_word=message)
		survey_responses = keyword.survey.all()
		for survey_response in survey_responses:
				survey_response_serializer = serializers.SurveyResponseSerializer()
				serialized_survey_response = survey_response_serializer.serialize([survey_response])
				serialized_survey_response = json.loads(serialized_survey_response)
				self.send_message(sender, serialized_survey_response[0]) 
		print(generic_responses)

	def parse_intent(self, sender, message):
		message = message.lower()
		if "@nswer_$urvey" == message:
				self.send_text_message(sender,"Thank you for answering the survey")
				self.survey(sender,message)
		elif "c0mm@nd_d0nt_s3nd_receipt" == message:
		 	self.do_check_out(sender)
		elif "c0mm@nd_cancel_check0ut" == message:
			self.send_text_message(sender, "Checkout was canceled.")
			self.send_response(sender, "c0mm@nd_sh0w_0pti0ns")
		elif "reserve jollibee" in message:
			self.send_message(sender, {'text':'For how many?'})
			self.save_keyword(sender, message)
		elif "c0mm@nd_add_to_cart_" in message:
			self.send_text_message(sender, "How many?")
			self.send_response(sender, 'c0mm@nd_sh0w_quantity_0pti0ns')
			self.save_keyword(sender, message)
		elif "c0mm@nd_clear_check0ut" == message:
			try:
				sender_obj = models.acebookUser.objects.get(messenger_id=sender)
				cart = models.VirtualCart.objects.get(user=sender_obj)
				cart.delete()
				self.send_message(sender, {'text' : 'Cart has now been cleared.'})
			except:
				self.send_message(sender, {'text' : 'Cart was already empty.'})
				self.send_response(sender, "c0mm@nd_sh0w_0pti0ns")
		else:
			try:
				previous_keyword = models.PreviousKeyword.objects.filter(user=sender).order_by('-date_sent')[0]
				previous_keyword = previous_keyword.key_word
				print ('previous_keyword: ' + previous_keyword)
				if "c0mm@nd_s3nd_receipt" in previous_keyword:
					try:
						validate_email(message)
						self.email_sender(sender,message)
						self.do_check_out(sender)
						self.save_keyword(sender, '')
					except:
						reply = "Email address is not valid"
						self.send_text_message(sender, reply)
				elif "c0mm@nd_add_to_cart_" in previous_keyword:
					try:
						quantity = int(message)
						code = previous_keyword.replace("c0mm@nd_add_to_cart_","")
						self.add_to_cart(sender, code, quantity)
					except:
						reply = "Please send a proper value."
						self.send_text_message(sender, reply)
						self.save_keyword(sender, 'error 123')
				elif 'reserve jollibee' in previous_keyword:
					try:
						quantity = int(message)
						user = models.FacebookUser.objects.get(messenger_id = sender)
						reply = "Created a reservation for " + user.first_name + ". Table for " + message + "."
						self.send_text_message(sender, reply)
					except:
						reply = "Please send a proper value."
						self.send_text_message(sender, reply)
						self.save_keyword(sender, 'error 123')
				else:
					self.send_response(sender, message)
			except:
				self.send_response(sender, message)

