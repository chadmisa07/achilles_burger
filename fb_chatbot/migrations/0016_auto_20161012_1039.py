# -*- coding: utf-8 -*-
# Generated by Django 1.10.2 on 2016-10-12 02:39
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('fb_chatbot', '0015_auto_20161012_1037'),
    ]

    operations = [
        migrations.AlterField(
            model_name='surveyreplylocation',
            name='content_type',
            field=models.CharField(default='location', editable=False, max_length=8),
        ),
    ]
