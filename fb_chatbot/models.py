import datetime

from django import forms
from django.db import models
from django.utils import timezone
from datetime import datetime


class FacebookUser(models.Model):
	messenger_id = models.IntegerField(verbose_name="Messengert ID", unique=True, default=0)
	first_name = models.CharField(max_length=250, verbose_name="First Name", blank=False)
	last_name = models.CharField(max_length=250, verbose_name="Last Name", blank=False)
	prof_pic = models.URLField(verbose_name="Profile Picture", blank=False)
	locale = models.CharField(max_length=20, verbose_name="Locale", blank=False)
	timezone = models.IntegerField(verbose_name="Timezone", blank=False)
	gender = models.CharField(max_length=6, verbose_name="Gender", blank=False)
	created_at = models.DateTimeField(auto_now_add=True, editable=False)
	updated_at = models.DateTimeField(auto_now=True, editable=False)
	email = models.EmailField(max_length=254, blank=True)

	def __str__(self):
		return self.first_name + " " + self.last_name + " (" + self.gender + ")"

	class Meta:
		verbose_name = "See Facebook User"
		verbose_name_plural = 'See Facebook Users'

class ProductCategory(models.Model):
	name = models.CharField(max_length=50)
	code = models.CharField(max_length=50, unique=True)
	description = models.CharField(max_length=250)

	def __str__(self):
		return self.name


class ProductItem(models.Model):
	name = models.CharField(max_length=50)
	code = models.CharField(max_length=50, unique=True)
	description = models.CharField(max_length=250)
	price = models.DecimalField(max_digits=20, decimal_places=2)
	category = models.ForeignKey(ProductCategory, default=None, blank=True)
	image_url = models.URLField(default='www.google.com.ph')

	def __str__(self):
		return self.name


class CartItem(models.Model):
	item = models.ForeignKey(ProductItem)
	quantity = models.IntegerField()

	def __str__(self):
		return self.item.name


class ProductOrder(models.Model):
	user = models.ForeignKey(FacebookUser, default=None, blank=True)
	date_ordered = models.DateTimeField(auto_now_add=True)
	item = models.ForeignKey(ProductItem)
	quantity = models.IntegerField(default=1)

	def __str__(self):
		return self.item.name + " / " + str(self.quantity)


class VirtualCart(models.Model):
	user = models.ForeignKey(FacebookUser, default=None, blank=True)
	products_added = models.ManyToManyField(CartItem, default=None, blank=True)


class ProductCategoryOrder(models.Model):
	category = models.ForeignKey(ProductCategory)
	order_count = models.IntegerField()

	def __str__(self):
		return self.category.name + " -- " + str(self.order_count)


class TextMessage(models.Model):
	text = models.CharField(max_length=320, verbose_name="Text", blank=False)
	created_at = models.DateTimeField(auto_now_add=True, editable=False)
	updated_at = models.DateTimeField(auto_now=True, editable=False)

	def __str__(self):
		return self.text

	class Meta:
		verbose_name = "Create Text Message Response"
		verbose_name_plural = 'Create Text Message Responses'


class AttachmentType(models.Model):
	attachment_type = models.CharField(max_length=10, verbose_name="Attachment Type", blank=False, editable=True)
	created_at = models.DateTimeField(auto_now_add=True, editable=False)
	updated_at = models.DateTimeField(auto_now=True, editable=False)

	def __str__(self):
		return self.attachment_type


class Attachment(models.Model):
	attachment_type = models.OneToOneField(AttachmentType, related_name="Type", default=None)
	title = models.CharField(max_length=50, verbose_name="Attachment Title", blank=False)
	url = models.URLField(verbose_name="Payload URL", blank=False)
	created_at = models.DateTimeField(auto_now_add=True, editable=False)
	updated_at = models.DateTimeField(auto_now=True, editable=False)

	def __str__(self):
		return self.title

	class Meta:
		verbose_name = "Create Attachment Response"
		verbose_name_plural = 'Create Attachment Responses'


class SurveyQuestion(models.Model):
	question = models.CharField(max_length=250, verbose_name="Question", blank=False, default="Question")
	yes_result = models.IntegerField(editable=False,blank=False, default=0)
	no_result = models.IntegerField(editable=False,blank=False, default=0)
	created_at = models.DateTimeField(auto_now_add=True, editable=False)
	updated_at = models.DateTimeField(auto_now=True, editable=False)
	

	def __str__(self):
		return self.question

	class Meta:
		verbose_name = "Create Survey Question"
		verbose_name_plural = "Create Survey Questions"


class SurveyTemplate(models.Model):
	description = models.CharField(max_length=250, verbose_name="Survey Description", blank=False)
	elements = models.ManyToManyField(SurveyQuestion, default=None, blank=True, verbose_name="Add Survey Question Elements")


	def __str__(self):
		return self.description

	class Meta:
		verbose_name = "Create Survey Template"
		verbose_name_plural = "Create Survey Template"

class SurveyReplyLocation(models.Model):
	content_type = models.CharField(max_length=8, default="location", blank=False, editable=False)
	confirmation = models.BooleanField(default=False, verbose_name="Confirm Add Share Location")
	survey_question = models.ForeignKey(SurveyQuestion, on_delete=models.CASCADE, blank=False)
	created_at = models.DateTimeField(auto_now_add=True, editable=False)
	updated_at = models.DateTimeField(auto_now=True, editable=False)

	def __str__(self):
		return self.content_type

class SurveyReplyText(models.Model):
	content_type = models.CharField(max_length=4, default="text", editable=False, blank=False)
	title = models.CharField(max_length=20, verbose_name="Title", blank=False, default="Title")
	payload = models.CharField(max_length=1000, verbose_name="Payload", blank=False, default="Payload")
	image_url = models.URLField(default=None, blank=True)
	survey_question = models.ForeignKey(SurveyQuestion, on_delete=models.CASCADE, blank=False)
	created_at = models.DateTimeField(auto_now_add=True, editable=False)
	updated_at = models.DateTimeField(auto_now=True, editable=False)

	def __str__(self):
		return self.title

class GenericTemplate(models.Model):
	title = models.CharField(max_length=90, verbose_name="Title", blank=False)
	subtitle = models.CharField(max_length=90, verbose_name="subtitle", blank=False)
	item_url = models.URLField(blank=False)
	image_url = models.URLField(blank=False)
	created_at = models.DateTimeField(auto_now_add=True, editable=False)
	updated_at = models.DateTimeField(auto_now=True, editable=False)

	def __str__(self):
		return self.title

	class Meta:
		verbose_name = "Create Generic Template"
		verbose_name_plural = "Create Generic Templates"


class GenericTemplatePostbackButton(models.Model):
	title = models.CharField(max_length=20, verbose_name="Title", blank=False)
	button_type = models.CharField(max_length=8, default="postback", blank=False, editable=False)
	payload = models.CharField(max_length=1000, verbose_name="Postback Message", blank=False)
	generic_template = models.ForeignKey(GenericTemplate, on_delete=models.CASCADE, default=None)
	created_at = models.DateTimeField(auto_now_add=True, editable=False)
	updated_at = models.DateTimeField(auto_now=True, editable=False)

	def __str__(self):
		return self.title + " " + self.button_type


class GenericTemplateCallButton(models.Model):
	title = models.CharField(max_length=20, verbose_name="Title", blank=False)
	button_type = models.CharField(max_length=12, default="phone_number", blank=False, editable=False)
	payload = models.CharField(max_length=30, verbose_name="Contact Number (eg. +15105551234)", blank=False)
	generic_template = models.ForeignKey(GenericTemplate, on_delete=models.CASCADE, default=None)
	created_at = models.DateTimeField(auto_now_add=True, editable=False)
	updated_at = models.DateTimeField(auto_now=True, editable=False)

	def __str__(self):
		return self.title + " " + self.button_type


class GenericTemplateShareButton(models.Model):
	button_type = models.CharField(max_length=12, default="element_share", blank=False, editable=True)
	confirm = models.BooleanField(default=False)
	generic_template = models.ForeignKey(GenericTemplate, on_delete=models.CASCADE, default=None)
	created_at = models.DateTimeField(auto_now_add=True, editable=False)
	updated_at = models.DateTimeField(auto_now=True, editable=False)

	def __str__(self):
		return "Do not delete. This is the default Share Button."


class GenericTemplateURLButtonHeight(models.Model):
	size = models.CharField(max_length=7, blank=False)
	created_at = models.DateTimeField(auto_now_add=True, editable=False)
	updated_at = models.DateTimeField(auto_now=True, editable=False)

	def __str__(self):
		return self.size


class GenericTemplateResponse(models.Model):
	title = models.CharField(max_length=50, verbose_name="Title", default="Title", blank=False)
	elements = models.ManyToManyField(GenericTemplate, default=None, blank=True, verbose_name="Add Generic Template Elements")
	created_at = models.DateTimeField(auto_now_add=True, editable=False)
	updated_at = models.DateTimeField(auto_now=True, editable=False)

	def __str__(self):
		return self.title

	class Meta:
		verbose_name = "Create Generic Template Response"
		verbose_name_plural = "Create Generic Template Responses"


class GenericTemplateURLButton(models.Model):
	button_type = models.CharField(max_length=7, default="web_url", blank=False, editable=False)
	title = models.CharField(max_length=20, verbose_name="Title", blank=False)
	url = models.URLField(default=None, blank=False)
	generic_template = models.ForeignKey(GenericTemplate, on_delete=models.CASCADE, default=None)
	webview_height_ratio = models.ForeignKey(GenericTemplateURLButtonHeight, related_name="Height", default=None)
	created_at = models.DateTimeField(auto_now_add=True, editable=False)
	updated_at = models.DateTimeField(auto_now=True, editable=False)

	def __str__(self):
		return "Do not delete. This is the default Share Button."


class ButtonResponse(models.Model):
	text = models.CharField(max_length=320, verbose_name="Text", blank=False)
	created_at = models.DateTimeField(auto_now_add=True, editable=False)
	updated_at = models.DateTimeField(auto_now=True, editable=False)

	def __str__(self):
		return self.text

	class Meta:
		verbose_name = "Create Button Response"
		verbose_name_plural = "Create Button Responses"


class ButtonResponsePostbackButton(models.Model):
	title = models.CharField(max_length=20, verbose_name="Title", blank=False)
	button_type = models.CharField(max_length=8, default="postback", blank=False, editable=False)
	payload = models.CharField(max_length=1000, verbose_name="Postback Message", blank=False)
	button_response = models.ForeignKey(ButtonResponse, on_delete=models.CASCADE, default=None)
	created_at = models.DateTimeField(auto_now_add=True, editable=False)
	updated_at = models.DateTimeField(auto_now=True, editable=False)

	def __str__(self):
		return self.title + " " + self.button_type


class ButtonResponseCallButton(models.Model):
	title = models.CharField(max_length=20, verbose_name="Title", blank=False)
	button_type = models.CharField(max_length=12, default="phone_number", blank=False, editable=False)
	payload = models.CharField(max_length=30, verbose_name="Contact Number (eg. +15105551234)", blank=False)
	button_response = models.ForeignKey(ButtonResponse, on_delete=models.CASCADE, default=None)
	created_at = models.DateTimeField(auto_now_add=True, editable=False)
	updated_at = models.DateTimeField(auto_now=True, editable=False)

	def __str__(self):
		return self.title + " " + self.button_type


class ButtonResponseURLButtonHeight(models.Model):
	size = models.CharField(max_length=7, blank=False)
	created_at = models.DateTimeField(auto_now_add=True, editable=False)
	updated_at = models.DateTimeField(auto_now=True, editable=False)

	def __str__(self):
		return self.size


class ButtonResponseURLButton(models.Model):
	button_type = models.CharField(max_length=7, default="web_url", blank=False, editable=False)
	title = models.CharField(max_length=20, verbose_name="Title", blank=False)
	url = models.URLField(default=None, blank=False)
	button_response = models.ForeignKey(ButtonResponse, on_delete=models.CASCADE, default=None)	
	webview_height_ratio = models.ForeignKey(ButtonResponseURLButtonHeight, related_name="Height", default=None)
	created_at = models.DateTimeField(auto_now_add=True, editable=False)
	updated_at = models.DateTimeField(auto_now=True, editable=False)

	def __str__(self):
		return self.title

class QuickReplyResponse(models.Model):
	text = models.CharField(max_length=50, verbose_name="Text or Question", blank=False)
	created_at = models.DateTimeField(auto_now_add=True, editable=False)
	updated_at = models.DateTimeField(auto_now=True, editable=False)

	def __str__(self):
			return self.text

	class Meta:
		verbose_name = "Create Quick Reply"
		verbose_name_plural = 'Create Quick Replies'


class QuickReplyLocation(models.Model):
	content_type = models.CharField(max_length=8, default="location", blank=False)
	confirmation = models.BooleanField(default=False, verbose_name="Confirm Add Share Location")
	quick_reply_response = models.ForeignKey(QuickReplyResponse, on_delete=models.CASCADE, blank=False)
	created_at = models.DateTimeField(auto_now_add=True, editable=False)
	updated_at = models.DateTimeField(auto_now=True, editable=False)

	def __str__(self):
		return self.content_type

class QuickReplyText(models.Model):
	content_type = models.CharField(max_length=4, default="text", editable=False, blank=False)
	title = models.CharField(max_length=20, verbose_name="Title", blank=False, default="Title")
	payload = models.CharField(max_length=1000, verbose_name="Payload", blank=False, default="Payload")
	image_url = models.URLField(default=None, blank=True)
	quick_reply_response = models.ForeignKey(QuickReplyResponse, on_delete=models.CASCADE, blank=False)
	created_at = models.DateTimeField(auto_now_add=True, editable=False)
	updated_at = models.DateTimeField(auto_now=True, editable=False)

	def __str__(self):
		return self.title


class Adjustment(models.Model):
	name = models.CharField(max_length=250)
	amount = models.DecimalField(max_digits=20, decimal_places=2)
	created_at = models.DateTimeField(auto_now_add=True, editable=False)
	updated_at = models.DateTimeField(auto_now=True, editable=False)

	def __str__(self):
		return self.name + " : " + str(self.amount)


class Address(models.Model):
	street_1 = models.CharField(max_length=250)
	street_2 = models.CharField(max_length=250, default=None, blank=True)
	city = models.CharField(max_length=250)
	postal_code  = models.CharField(max_length=250)
	state = models.CharField(max_length=250)
	country = models.CharField(max_length=250)
	created_at = models.DateTimeField(auto_now_add=True, editable=False)
	updated_at = models.DateTimeField(auto_now=True, editable=False)

	def __str__(self):
		return self.street_1 + ", " + self.city + ", " + self.country


class ReceiptElement(models.Model):
	title = models.CharField(max_length=250)
	subtitle = models.CharField(max_length=250)
	quantity = models.IntegerField()
	price = models.DecimalField(max_digits=20, decimal_places=2)
	currency = models.CharField(max_length=250, default='PHP')
	image_url = models.URLField(default='https://www.google.com')
	created_at = models.DateTimeField(auto_now_add=True, editable=False)
	updated_at = models.DateTimeField(auto_now=True, editable=False)

	def __str__(self):
		return self.title


class ReceiptResponse(models.Model):
	recipient_name = models.CharField(max_length=250)
	recipient_address = models.ForeignKey(Address, default=None, blank=True)
	order_number = models.IntegerField()
	currency = models.CharField(max_length=250, default='PHP')
	payment_method = models.CharField(max_length=250)
	order_url = models.URLField(default='https://www.google.com')
	timestamp = models.DateTimeField(auto_now_add=True)
	elements = models.ManyToManyField(ReceiptElement, default=None, blank=True)
	adjustments = models.ManyToManyField(Adjustment, default=None, blank=True)
	subtotal = models.DecimalField(max_digits=20, decimal_places=2, blank=True, default=None)
	shipping_cost = models.DecimalField(max_digits=20, decimal_places=2, blank=True, default=None)
	total_tax = models.DecimalField(max_digits=20, decimal_places=2, blank=True, default=None)
	total_cost = models.DecimalField(max_digits=20, decimal_places=2, blank=True, default=None)
	created_at = models.DateTimeField(auto_now_add=True, editable=False)
	updated_at = models.DateTimeField(auto_now=True, editable=False)

	def __str__(self):
		return self.recipient_name + " " + str(self.order_number)


class PreviousKeyword(models.Model):
	key_word = models.CharField(max_length=250, verbose_name="Key Word")
	user = models.IntegerField(blank=False)
	date_sent = models.DateTimeField(auto_now_add=True, editable=False)

	def __str__(self):
		return self.key_word + ' / Sent:' + str(self.date_sent)


class Keyword(models.Model):
	key_word = models.CharField(max_length=250, unique=True, verbose_name="Key Word")
	text_message = models.ManyToManyField(TextMessage, default=None, blank=True, verbose_name="Add Text Response")
	attachment = models.ManyToManyField(Attachment, default=None, blank=True, verbose_name="Add Attachment Response")
	generic_response = models.ManyToManyField(GenericTemplateResponse, default=None, blank=True, verbose_name="Add Generic Template")
	receipt_response = models.ManyToManyField(ReceiptResponse, default=None, blank=True, verbose_name="Add Receipt Reply")
	quick_reply = models.ManyToManyField(QuickReplyResponse, default=None, blank=True, verbose_name="Add Quick Reply")
	survey = models.ManyToManyField(SurveyTemplate, default=None, blank=True, verbose_name="Add Survey")
	button_response  = models.ManyToManyField(ButtonResponse, default=None, blank=True, verbose_name="Add Button Response")
	children = models.ForeignKey('self', null=True, blank=True, default=None, verbose_name='Add Child Keywords')

	def __str__(self):
		return self.key_word

	class Meta:
		verbose_name = 'Keyword'
		verbose_name_plural = 'Keywords'
