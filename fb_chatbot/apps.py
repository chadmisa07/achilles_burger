from django.apps import AppConfig


class FbChatbotConfig(AppConfig):
    name = 'fb_chatbot'
    verbose_name = 'Facebook Chatbot'
