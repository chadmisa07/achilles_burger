from django.conf.urls import url, include
from . import views

urlpatterns = [
    url(r'view_cart/', views.view_cart, name='view_cart'),
]